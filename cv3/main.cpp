#include <iostream>
using namespace std;

class Developer{
private:
    string m_jmeno;
    int m_mzda;
    float m_odpracovaneHodiny;
    int m_bonus;
    int m_pocetChyb;
    int m_srazkaZaChybu;

public:
    Developer(string jmeno, int mzda){
        m_jmeno = jmeno;
        m_mzda = mzda;
        m_bonus = 2*mzda;
        m_odpracovaneHodiny = 0;
        m_pocetChyb = 0;
        m_srazkaZaChybu = 100;

    }
    Developer(string jmeno, int mzda, float odpracovano, int bonus){
        m_jmeno = jmeno;
        m_mzda = mzda;
        m_odpracovaneHodiny = odpracovano;
        m_bonus = bonus;
        m_pocetChyb = 0;
        m_srazkaZaChybu = 100;
    }

    void printInfo(){
        cout << "Jsem developer" << m_jmeno;
        cout << " odpracoval jsem " << m_odpracovaneHodiny;
        cout << " udelal jsem " << m_pocetChyb;
        cout << " moje vyplata bude " << getMzda() << endl;
    }

    void evidujChybu(){
        m_pocetChyb += 1;
    }

    void pracuj(float kolikHodin){
        m_odpracovaneHodiny += kolikHodin;
    }

    void novyMesic(){
        m_odpracovaneHodiny = 0;
        m_pocetChyb = 0;
    }

    void setMzda(int novaMzda){
        m_mzda = novaMzda;
    }

    int getMzda(){
        int vyplata = 0;
        // vypocet mzdy
        if(m_odpracovaneHodiny < 40){
            vyplata = m_mzda * m_odpracovaneHodiny;
        }
        else{
            vyplata =  m_mzda * 40;
            vyplata += m_bonus * (m_odpracovaneHodiny-40);
        }
        // srazky za chyby z platu
        vyplata -= m_pocetChyb * m_srazkaZaChybu;
        return vyplata;

    }
};


int main() {
    Developer* d = new Developer("Mikulas", 100);
    d->pracuj(50);
    d->evidujChybu();
    d->evidujChybu();
    cout << d->getMzda() << endl;
    d->printInfo();
    return 0;
}
