#include <iostream>

using namespace std;

class Student{
public:
    string m_jmeno;
    string m_rc;
    string m_adresa;

    Student(string jmeno, string rc, string adresa){
        m_jmeno = jmeno;
        m_rc = rc;
        m_adresa = adresa;
    }

    //Student(string jmeno, string rc) : Student(jmeno, rc, "nezmama") {
    // }

    Student(string jmeno, string rc){
        m_jmeno = jmeno;
        m_rc = rc;
        m_adresa = "neznama";
    }
    

    void setAdresa(string adresa){
        m_adresa = adresa;
    }

    string getJmeno(){
        return m_jmeno;
    }

    string getRC(){
        return m_rc;
    }

    string getAdresa(){
        return m_adresa;
    }

    void printInfo(){
        cout << "Jsem student " << getJmeno();
        cout << " a moje rc je " << getRC();
        cout << ". Bydlim na " << getAdresa() << endl;
    }

};

int main() {
    Student* s = new Student("Frantisek","6165/515", "Krokova 12"));
    s->printInfo();
    s->setAdresa("Krokova");
    s->printInfo();
    return 0;
}
